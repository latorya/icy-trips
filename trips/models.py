from django.db import models
from django.contrib.auth.models import User

class Trip(models.Model):

    FUTURE_TRIP = 'future'
    PAST_TRIP = 'past'

    TRIP_CHOICES = [
        (FUTURE_TRIP, 'Future Trip'),
        (PAST_TRIP, 'Past Trip'),
    ]
    title = models.CharField(max_length=200)
    picture = models.URLField(blank=True)
    description = models.TextField()
    location = models.CharField(max_length=100)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    future_trip = models.BooleanField(default=False)
    past_trip = models.BooleanField(default=False)
    trip_type = models.CharField(max_length=10, choices=TRIP_CHOICES, default=FUTURE_TRIP)

    def __str__(self):
        return self.title

